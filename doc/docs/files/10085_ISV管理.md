# ISV管理

ISV：独立软体开发商（independent software vendor），即接入方或者说接口调用者，在SOP中称为ISV。

---

在1.1.0版本中新增了ISV管理功能，在sop-admin中ISV管理模块下。功能如下：

- 基本信息的增查改
- 设置对应角色

界面如下图所示：

![admin预览](images/10085_1.png "10085_1.png")
