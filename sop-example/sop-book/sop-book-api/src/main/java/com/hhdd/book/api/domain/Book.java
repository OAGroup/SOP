package com.hhdd.book.api.domain;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class Book {
    private int id;
    private String name;
}
